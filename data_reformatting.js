// this script takes the data in its flourish format
// to transform it into easy to use data
var treatedProjects = []
var reformatted_data = []
_Flourish_data.data.forEach((line) => {
	let project = line.nest_columns[3]
	if (!treatedProjects.includes(project)) {
		_Flourish_data.data.filter((line) => {
			return line.nest_columns[3] === project
		}).forEach((line) => {
			const newLine = {
				'projet': project,
				'spé': line.filter,
				'behavior': line.nest_columns[2],
				'skill': line.nest_columns[1],
				'domain': line.nest_columns[0]
			}
			reformatted_data.push(newLine)
		})
		treatedProjects.push(project)
	}
})

JSON.stringify(reformatted_data)
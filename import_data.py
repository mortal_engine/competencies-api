import os
import json
from dotenv import load_dotenv
from pymongo import MongoClient, InsertOne

load_dotenv()

client = MongoClient(os.environ['MONGODB_URL'])
db = client.gandalf
collection = db.competencies
requesting = []

with open(r"competencies.json", encoding='utf-8') as f:
    for jsonObj in f:
        jsonData = json.loads(jsonObj)
        for line in jsonData:
            requesting.append(InsertOne(line))

result = collection.bulk_write(requesting)
client.close()
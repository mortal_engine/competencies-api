import os
from typing import List

import motor.motor_asyncio
from bson import ObjectId
from dotenv import load_dotenv
from fastapi import FastAPI
from pydantic import BaseModel, Field

load_dotenv()

api_description = """
This API allows epitech students to have access to a hassle-free list of competencies on a per-project basis.
"""

app = FastAPI(
    title='competencies-api',
    description=api_description,
    version='1.0',
    license_info={
        'name': 'The MIT License (MIT)', 
        'url' : 'https://mit-license.org/'
    },
)
mongo_client = motor.motor_asyncio.AsyncIOMotorClient(os.environ['MONGODB_URL'])
db = mongo_client.gandalf

class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError('Invalid objectid')
        return ObjectId(v)

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type='string')

class CompetencyModel(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias='_id')
    behavior: str = Field(...)
    domain: str = Field(...)
    projet: str = Field(...)
    skill: str = Field(...)
    spé: str = Field(...)

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "projet": "T-PROJET-001",
                "spé": "SPE",
                "behavior": "0.1.B01 - Behavior",
                "skill": "0.1 Skill",
                "domain": "0. Domain"
            }
        }

@app.get(
    '/', 
    response_description='List competencies corresponding to a given project of specific specialization',
    response_model=List[CompetencyModel]
)
async def list_competencies(project: str, spe: str):
    competencies = await db['competencies'].find({'projet': project, 'spé': spe}).to_list(100)
    return competencies

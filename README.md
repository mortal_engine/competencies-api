# Getting started
You will need a running [mongodb](https://www.mongodb.com/) instance.

## Configuration
See .env.example, set variables to desired value and rename to .env

## Populating the DB
```bash
python import_data.py
```
## Running
```bash
docker build . -t api_image
```
```bash
docker run -d -p 80:80 api_image
```

# How
The iframe containing the flourish graph for the competencies is located here : https://flo.uri.sh/visualisation/8474060/embed?auto=1

The js variable _Flourish_data contains the raw data for the competency framework, which can be reformatted and extracted executing data_reformatting.js. The result is manually exported into competencies.json.

# Why
To be able to access competencies on a per-project basis instead of the other way around.